export enum TrackerType {
  HTTP = 1,
  UDP = 2,
  DHT = 3,
}

export interface Tracker {
  url: string,
  isEnabled: boolean,
  isOpen: boolean,
  type: TrackerType,
  seeders: number,
  leechers: number,
  numberDownloaded: number,
}

export interface File {
  name: string,
  length: number,
  isComplete: boolean,
}

export interface Torrent {
  name: string,
  hash: string,
  path: string,
  torrentFile: string,
  isOpen: boolean,
  isActive: boolean,
  isComplete: boolean,
  ratio: number,
  length: number,
  downloaded: number,
  uploaded: number,
  downRate: number,
  upRate: number,
  freeSpace: number,
  createdAt: string,
  startedAt: string,
  finishedAt: string,
  loadedAt: string,
  stateChangedAt: string,
  trackers: Tracker[],
  files: File[],
  peersConnected?: number,
  peersCompleted?: number,
  totalSeeders?: number,
  totalLeechers?: number,
}
