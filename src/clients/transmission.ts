// @ts-ignore
import TorrentClientInterface from '../TorrentClientInterface';
import { File, Torrent, Tracker } from '../types';

// tslint:disable-next-line:no-var-requires
const Transmission = require('transmission');

class Client implements TorrentClientInterface {
  client;

  constructor(host: string, port: number, endpoint: string, user?: string, password?: string) {
    this.client = new Transmission({
      host,
      port: port || 9091,
      endpoint: endpoint || '/transmission/rpc',
      username: user || null,
      password: password || null,
    });
  }

  getVersion(): Promise<string> {
    return new Promise((resolve, reject) => {
      this.client.get((err: any) => {
        if (err) {
          reject(err);
        }
        else {
          resolve('Transmission');
        }
      });
      setTimeout(() => {reject(new Error('Timeout'))}, 2000);
    });
  }

  async transform(torrent: any): Promise<Torrent> {
    const data: Torrent = {
      hash: torrent.hashString.toUpperCase(),
      name: torrent.name,
      path: '',
      torrentFile: torrent.torrentFile,
      isOpen: [3,4,5,6].includes(torrent.status),
      isActive: [3,4,5,6].includes(torrent.status),
      isComplete: Number(torrent.sizeWhenDone) === Number(torrent.downloadedEver),
      ratio: torrent.uploadRatio,
      length: Number(torrent.sizeWhenDone),
      downloaded: Number(torrent.downloadedEver),
      uploaded: Number(torrent.uploadedEver),
      downRate: parseInt(torrent.rateDownload, 10),
      upRate: parseInt(torrent.rateUpload, 10),
      freeSpace: torrent.downloadDir ? await this.freeSpace(torrent.downloadDir) : 0,
      createdAt: (new Date(torrent.addedDate * 1000)).toJSON(),
      startedAt: (new Date(torrent.startDate * 1000)).toJSON(),
      finishedAt: (new Date(torrent.doneDate * 1000)).toJSON(),
      loadedAt: (new Date(torrent.addedDate * 1000)).toJSON(),
      stateChangedAt: null,
      trackers: [],
      files: [],
    };

    data.files = torrent.files.map((f: any) => ({
      name: f.name,
      length: f.length,
      isCompleted: f.bytesCompleted === f.length,
    }))

    data.trackers = torrent.trackerStats.map(this.transformTracker);
    data.totalSeeders = data.trackers.reduce((prev, next) => prev + next.seeders, 0);
    data.totalLeechers = data.trackers.reduce((prev, next) => prev + next.leechers, 0);

    return data;
  }

  transformTracker(tracker: any): Tracker {
    return {
      url: tracker.announce,
      type: 0,
      isEnabled: tracker.hasScraped,
      isOpen: tracker.hasAnnounced,
      seeders: tracker.seederCount,
      leechers: tracker.leecherCount,
      numberDownloaded: tracker.downloadCount,
    };
  }

  async getHashes(): Promise<string[]> {
    return new Promise((resolve, reject) => {
      this.client.get(async(err: any, result: any) => {
        if (err) {
          reject(err);
        } else {
          resolve(result.torrents.map((torrent: any) => torrent.hash))
        }
      });
    });
  }

  async get(): Promise<Torrent[]> {
    return new Promise((resolve, reject) => {
      this.client.get(async(err: any, result: any) => {
        if (err) {
          reject(err);
        } else {
          resolve(Promise.all(result.torrents.map((torrent: any) => this.transform(torrent))));
        }
      });
    });
  }

  async getOne(hash: string): Promise<Torrent> {
    const self = this;
    return new Promise((resolve, reject) => {
      this.client.get(hash, (err: any, result: any) => {
        if (err) {
          reject(err);
        }
        else {
          resolve(self.transform(result.torrents[0]));
        }
      });
    });
  }

  freeSpace(path: string): Promise<number> {
    return new Promise((resolve, reject) => {
      this.client.freeSpace(path, (err: any, result: any) => {
        if (err) {
          reject(err);
        }
        else {
          resolve(result['size-bytes']);
        }
      });
    });
  }

  async getFiles(hash: string): Promise<File[]> {
    return new Promise((resolve, reject) => {
      this.client.get(hash, (err: any, result: any) => {
        if (err) {
          reject(err);
        }
        else {
          resolve(result.torrents[0].files.map((f: any) => ({
            name: f.name,
            length: f.length,
            isComplete: f.bytesCompleted === f.length,
          })));
        }
      });
    });
  }

  async getTrackers(hash: string): Promise<Tracker[]> {
    return new Promise((resolve, reject) => {
      this.client.get(hash, (err: any, result: any) => {
        if (err) {
          reject(err);
        }
        else {
          resolve(result.torrents[0].trackers.map(this.transformTracker));
        }
      });
    });
  }

  async start(hash: string): Promise<boolean> {
    return this.play(hash);
  }

  async stop(hash: string): Promise<boolean> {
    return this.pause(hash);
  }

  async play(hash: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.client.start(hash, (err: any, arg: any) => {
        if (err) {
          reject(err);
        }
        else {
          resolve(arg);
        }
      });
    });
  }

  async pause(hash: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.client.stop(hash, (err: any, arg: any) => {
        if (err) {
          reject(err);
        }
        else {
          resolve(arg);
        }
      });
    });
  }

  async remove(hash: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.client.remove(hash, false, (err: any, arg: any) => {
        if (err) {
          reject(err);
        }
        else {
          resolve(arg);
        }
      });
    });
  }

  async createFromFile(filePath: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.client.addFile(filePath, (err: any, arg: boolean) => {
        if (err) {
          reject(err);
        } else {
          resolve(arg);
        }
      });
    });
  }

  async createFromBuffer(buffer: any): Promise<boolean> {
    throw new Error('"createFromBuffer" is not supported');
  }
}

module.exports = Client;
