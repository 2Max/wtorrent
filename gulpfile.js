const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const ts = require('gulp-typescript');
const tslint = require("gulp-tslint");
const tsProject = ts.createProject('./tsconfig.json', { declaration: true });

const srcPath = './src';
const distPath = './dist';

gulp.task('ts:dev', function () {
  return tsProject.src()
    .pipe(sourcemaps.init({}))
    .pipe(tsProject()).js
    .pipe(sourcemaps.write(
      '.',
      {
        includeContent: false,
        sourceRoot: '../',
      }
    ))
    .pipe(gulp.dest(distPath));
});

gulp.task('tslint:prod', function() {
  return gulp.src(`${srcPath}/**/*.ts`, { base: '.' })
    .pipe(tslint({
      fix: true,
    }))
    .pipe(tslint.report());
})

gulp.task('ts:prod', function () {
  const tsResult = tsProject.src()
    .pipe(tsProject());

  return tsResult
    .pipe(gulp.dest(distPath));
});

gulp.task('watch', function () {
  gulp.watch([
    './index.ts',
    './test.ts',
    `${srcPath}/**/*.ts`
  ], gulp.series('ts:dev'));
});

gulp.task(
  'build',
  gulp.series(
    'tslint:prod',
    'ts:prod'
  ),
)

gulp.task(
  'default',
  gulp.parallel(
    gulp.series(
      'tslint:prod', 'ts:dev'
    ),
    'watch'
  )
)
